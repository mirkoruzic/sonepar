# Start from the latest golang base image
FROM golang:latest

# Set the Current Working Directory inside the container
WORKDIR /app

# Copy go main file to workdir directory
COPY main.go ./

#Get Go dependencies

RUN go get -u github.com/gorilla/mux
RUN go get -u gopkg.in/natefinch/lumberjack.v2

# Build Go app
RUN go build -o main .

# Expose port 8080 to the outside world
EXPOSE 8000

# Command to run the executable
CMD ["./main"]

