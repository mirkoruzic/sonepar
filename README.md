# Simple Rest Api mocked application with GET, POST, PUT and DELETE requests develop in Go

Rest Api mocked application and CD/CD gitlab configuration is located in the root of the projects.

Terraform configuration for K8S deployment on Azure is in **terraform** directory.

K8S deployment template is in **k8s-template** directory.

Application is running on MS Azure K8S cluster: http://13.93.118.244/api

Project is integrated with GitLab CI/CD. Every commit-push in the repo will triger CI/CD pipeline with deployment to MS Azure K8S

## Rest-API application

- Install Go - OS Ubuntu 18.04:

```
    - sudo apt-get update
    - sudo apt-get -y upgrade
    - wget https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz
    - sudo tar -xvf go1.12.7.linux-amd
    - sudo mv go /usr/local
    - mkdir -p $HOME/go/src/projects $HOME/go/bin
    - vim ~/.bashrc
        - export GOPATH=$HOME/go
        - export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin64.tar.gz
    - go version
        - go version go1.12.7 linux/amd64
    - go env
```

- Build app localy:

```
    - cd $HOME/go/src/projects
    - git clone git@gitlab.com:mirkoruzic/sonepar.git
    - cd sonepar/
    - go get -u github.com/gorilla/mux
    - go get -u gopkg.in/natefinch/lumberjack.v2
    - go build 
    - ./sonepar &
    - curl http://localhost:8000/api -> will get all posts
    
    - e.g. send POST Request => curl --header "Content-Type: application/json" --request POST \
  --data '{"title":"Sonepar simple rest-api","body":"Testing simple rest api - second post"}' \
  http://localhost:8000/api

    - e.g. send DELETE Request => curl --header "Content-Type: application/json" --request DELETE http://localhost:8000/api/${id}
```

- Docker build:
    - Assuming docker is installed on local machine

    ```
        - build: docker build -t sample .
        - run: docker run -d -p 8000:8000 sample:latest
        - stop: docker container stop ${CONTAINER_ID}
    ```

- Docker image push to Docker Hub public repo
```
    - Login to docker hub
    - Tag image: docker tag sample:latest ruzicmirko/go-rest-api:latest
    - Push to docker hub: docker push ruzicmirko/go-rest-api:latest
```

##Install terraform if it's not already installed using the following steps:

- Browsing the following link and download terraform:
    https://www.terraform.io/downloads.html
- Extract your downloaded zip file
- For Ubuntu users:
    - Run the following command:
        ```bash
        sudo mv terraform /usr/local/bin/
        ```
Verify your installation using the command:
```bash
terraform -v
```
## Configure Terraform with MS Azure - Debian

```
- curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
- sudo apt-get update
- sudo apt-get install ca-certificates  curl apt-transport-https lsb-release gnupg
- curl -sL https://packages.microsoft.com/keys/microsoft.asc | \
    gpg --dearmor | \
    sudo tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null
- AZ_REPO=$(lsb_release -cs)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | \
    sudo tee /etc/apt/sources.list.d/azure-cli.list
- sudo apt-get update
- sudo apt-get install azure-cli
- az login
```

## Setup Terraform to access Azure

```
- az account show --query "{subscriptionId:id, tenantId:tenantId}"
- az account set --subscription="${SUBSCRIPTION_ID}"
- az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/${SUBSCRIPTION_ID}"
```

## Setup terraform environment variable for Your service principal

This will export in your current terminal session. For permanent access you can add in ~/.bashrc

```
- export ARM_SUBSCRIPTION_ID=your_subscription_id
- export ARM_CLIENT_ID=your_appId
- export ARM_CLIENT_SECRET=your_password
- export ARM_TENANT_ID=your_tenant_id
```

## Apply terraform k8s configuration on MS Azure

Note: this configuration is already applied on my MS Azure account. Some of the services, their names, requried to be unique on a public level. if you apply configuration, most probably you will get "not unique" error

Kubernetes cluster is setup in a West Europe region

```

- cd terraform/azure/k8s
- az storage container create -n tfstate --account-name <YourAzureStorageAccountName> --account-key <YourAzureStorageAccountKey>
- terraform init -backend-config="storage_account_name=<YourAzureStorageAccountName>" -backend-config="container_name=tfstate" -backend-config="access_key=<YourStorageAccountAccessKey>" -backend-config="key=codelab.microsoft.tfstate"

- export TF_VAR_client_id=<your-client-id>
- export TF_VAR_client_secret=<your-client-secret>
- terraform plan -out out.plan
- terraform apply out.plan
```

## Test Azure Kubernetes cluster

```
- echo "$(terraform output kube_config)" > ./config
- export KUBECONFIG=./config
- check nodes: kubectl get nodes
```

## Deploy Rest-API application to K8S

```
- cd k8s-template/
- kubectl apply -f rest-api.yml
- verify: kubectl get pods --namespace default; kubectl get services --namespace default
```